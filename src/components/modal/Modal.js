import React from "react";
import "./Modal.scss"

class Modal extends React.Component {
    render() {
        const {text, onClick, header, closeButton, bgcolor, action} = this.props;
        return (
            <div onClick={onClick} className="ModalBg">
                <div className="Modal" style={{backgroundColor: bgcolor}} onClick={this.preventCloseModal}>
                    <div className="Header">
                        {header}
                        {closeButton && <div className="Cross" onClick={onClick}></div>}
                    </div>
                    <div className="Main_content">
                        <p>{text}</p>
                    </div>
                    {action}
                </div>
            </div>
        )
    }

    preventCloseModal = (e) => {
        console.log(e.target);
        e.stopPropagation();
    }
}

export default Modal;