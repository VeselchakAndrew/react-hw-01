import React, {Component} from 'react';
import "./Button.scss"

class Button extends Component {

    render() {

        const {name, bgcolor, onClick, buttonClass} = this.props;
        return (

            <button className={buttonClass} style={{backgroundColor: bgcolor}} onClick={onClick}>{name}</button>
        );
    }
}

export default Button;