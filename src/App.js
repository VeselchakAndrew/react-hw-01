import React from 'react';
import './App.scss';
import Button from "./components/button/button";
import Modal from "./components/modal/Modal";


class App extends React.Component {
    state = {
        isOpenModalWindowFirst: false,
        isOpenModalWindowSecond: false,

    }


    render() {

        const modalText1 = "Once you delete this file, it won’t be possible to undo this action. \n" +
            "Are you sure you want to delete it?";
        const modalHeader1 = "Do you want to delete this file?";
        const modalText2 = "Let's rock and roll!";
        const modalHeader2 = "Are you ready for the rock?";

        return (
            <>
                <div className="App">
                    <Button buttonClass="button" bgcolor="red" onClick={this.showFirstModal} name="Open first modal"/>
                    <Button buttonClass="button" bgcolor="green" onClick={this.showSecondModal}
                            name="Open second modal"/>
                </div>

                {this.state.isOpenModalWindowFirst &&
                <Modal header={modalHeader1}
                       text={modalText1}
                       closeButton={true}
                       onClick={this.closeModals}
                       bgcolor="#e74c3c"
                       action={
                           <>
                               <Button buttonClass="modal_button" bgcolor="red" onClick={this.closeModals} name="OK"/>
                               <Button buttonClass="modal_button" bgcolor="red" onClick={this.closeModals}
                                       name="Cancel"/>
                           </>
                       }
                />
                }
                {this.state.isOpenModalWindowSecond &&
                <Modal header={modalHeader2}
                       text={modalText2}
                       closeButton={false}
                       onClick={this.closeModals}
                       bgcolor="#13c953"
                       action={
                           <>
                               <Button buttonClass="modal_button" bgcolor="green" onClick={this.closeModals} name="OK"/>
                               <Button buttonClass="modal_button" bgcolor="green" onClick={this.closeModals}
                                       name="Cancel"/>
                           </>
                       }/>
                }

            </>
        );

    }

    showFirstModal = () => {
        this.setState({isOpenModalWindowFirst: true})
        console.log(this.state);
    }

    showSecondModal = () => {
        this.setState({isOpenModalWindowSecond: true})
        console.log(this.state);
    }

    closeModals = (e) => {
       this.setState({
            isOpenModalWindowFirst: false,
            isOpenModalWindowSecond: false,
        })
    }


}

export default App;
